package com.lksoft.nintamafighter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.lksoft.nintamafighter.data.KeySettingsData;
import com.lksoft.nintamafighter.data.KeySettingsDataSerializer;

/**
 * Created by lake on 15/08/08.
 */
public class Context {
    // Singleton
    static Context self;
    public static Context get(){ return self; }

    // Game
    public Game game;

    // Assets manager
    public AssetManager assetManager = new AssetManager();

    // Json to parse data
    public Json json = new Json();

    // Viewport
    public Viewport viewport;

    /**
     * Create new context for game
     * @param game
     */
    public Context(Game game) {
        this.game = game;

        // Setup json
        json.setIgnoreUnknownFields(true);
        json.setSerializer(KeySettingsData.class, new KeySettingsDataSerializer());
    }

}
