package com.lksoft.nintamafighter.battle;

import com.badlogic.gdx.math.Rectangle;
import com.lksoft.nintamafighter.data.FighterData;
import com.lksoft.nintamafighter.data.PlaceData;
import com.lksoft.nintamafighter.data.SettingsData;

/**
 * Created by lake on 15/07/26.
 */
public class BattleState {

    // State
    private Fighter fighter1;
    private Fighter fighter2;
    private Background bg;

    /**
     * Create a new battle state for specified place and fighters
     * @param place
     * @param f1
     * @param f2
     * @param settings
     */
    public BattleState(PlaceData place, FighterData f1, FighterData f2, SettingsData settings){
        this.bg = new Background(place);

        // Set initial positions
        float y = 0;
        float x1 = bg.getWidth()/2 - 100;
        float x2 = bg.getWidth()/2 + 100;

        this.fighter1 = new Fighter(this, f1, x1, y, settings.p1);
        this.fighter2 = new Fighter(this, f2, x2, y, settings.p2);
    }

    /**
     * Progress game
     */
    public void update() {
        // Update bg
        getBg().update(getFighter1(), getFighter2());

        // Update fighters
        getFighter1().update();
        getFighter2().update();

        // Adjust position not to overlap!
        adjustOverlap();
    }

    public Fighter getFighter1() {
        return fighter1;
    }

    public Fighter getFighter2() {
        return fighter2;
    }

    public Background getBg() {
        return bg;
    }

    /**
     * Get the fighter that is not me
     * @param me
     * @return
     */
    public Fighter getOther(Fighter me){
        if( getFighter1() == me ) return getFighter2();
        else return getFighter1();
    }


    /**
     * Adjust the position so that fighters wouldn't overlap with each other
     */
    private void adjustOverlap() {
        // Set correct brect position
        float x = getFighter1().getPosition().x - getFighter1().getBrect().width/2;
        float y = getFighter1().getPosition().y + getFighter1().getBrect().height/2;
        getFighter1().getBrect().setPosition(x, y);

        x = getFighter2().getPosition().x - getFighter2().getBrect().width/2;
        y = getFighter2().getPosition().y + getFighter2().getBrect().height/2;
        getFighter2().getBrect().setPosition(x, y);

        // Collision
        Rectangle r1 = getFighter1().getBrect();
        Rectangle r2 = getFighter2().getBrect();
        if( r1.overlaps(r2) ){
            float overlapWidth = r1.x < r2.x ? r1.width - (r2.x - r1.x) : r2.width - (r1.x - r2.x);
            float step = 4 + overlapWidth/4;

            float f1Step = 0;
            float f2Step = 0;

            // Push back!
            if( r1.x < r2.x ){
                f1Step = -step/2;
                f2Step = step/2;
            } else {
                f1Step = step/2;
                f2Step = -step/2;
            }

            getFighter1().getPosition().x += f1Step;
            getFighter2().getPosition().x += f2Step;
        }
    }
}
