package com.lksoft.nintamafighter.battle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.lksoft.nintamafighter.Context;
import com.lksoft.nintamafighter.NintamaFighter;
import com.lksoft.nintamafighter.data.LayerData;

/**
 * Created by lake on 15/07/26.
 */
public class BattleRenderer {
    // References
    private Viewport viewport;
    private BattleState battleState;

    // Sprite batch
    private SpriteBatch batch;
    // Shape renderer
    private ShapeRenderer shapeRenderer;


    // Render overlay
    private boolean renderOverlay = true;

    /**
     * Create a new battle renderer
     * @param viewport
     * @param battleState
     */
    public BattleRenderer(Viewport viewport, BattleState battleState) {
        this.viewport = viewport;
        this.battleState = battleState;
        this.batch = new SpriteBatch();
        this.shapeRenderer = new ShapeRenderer();
    }

    /**
     * Render
     */
    public void render() {
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();

        // Render bg
        battleState.getBg().render(batch);

        // Render fighters
        battleState.getFighter1().render(batch);
        battleState.getFighter2().render(batch);

        batch.end();

        // Render overlays for collision debugging
        if( renderOverlay ) {
            shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

            battleState.getFighter1().renderOverlay(shapeRenderer);
            battleState.getFighter2().renderOverlay(shapeRenderer);

            shapeRenderer.end();
        }
    }

    /**
     * Update camera position
     */
    public void updateCamera() {
        viewport.getCamera().position.set(
                battleState.getBg().getWidth() / 2 + battleState.getBg().getScrollPosition(),
                Context.get().viewport.getWorldHeight() / 2, 0);

        viewport.getCamera().update();
    }

    /**
     * @return Whether render overlay is active
     */
    public boolean isRenderOverlay() {
        return renderOverlay;
    }

    /**
     * @param renderOverlay true to render collision and brects overlays
     */
    public void setRenderOverlay(boolean renderOverlay) {
        this.renderOverlay = renderOverlay;
    }
}
