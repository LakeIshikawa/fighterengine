package com.lksoft.nintamafighter.battle;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.lksoft.nintamafighter.data.FighterData;
import com.lksoft.nintamafighter.data.UserSettingsData;

/**
 * Created by lake on 15/07/28.
 */
public class Fighter {
    // Data
    private FighterData data;
    // User setings
    private UserSettingsData settings;
    // Fighter machine
    private FighterMachine machine;
    // Position
    private Vector2 position = new Vector2();
    // Flip state
    private boolean flip;

    // Brect
    private Rectangle brect = new Rectangle(0, 0, 40, 80);
    // Whether fighter is pushing towards opponent
    private boolean pushing = false;

    // Next state for next frame!
    private State<Fighter> nextState;

    // References
    private BattleState battleState;

    /**
     * Create a new fighter by data
     * @param data
     */
    public Fighter(BattleState battleState, FighterData data, float x, float y, UserSettingsData settings){
        this.settings = settings;
        this.battleState = battleState;
        this.data = data;
        this.position.set(x, y);
        machine = new FighterMachine(this, data);
        machine.setInitialState(machine.getState("idle"));
    }

    /**
     * @return Current position
     */
    public Vector2 getPosition(){
        return position;
    }

    /**
     * @return Whether the character is flipped or not
     */
    public boolean isFlip(){
        return flip;
    }

    /**
     * @return User settings
     */
    public UserSettingsData getSettings(){
        return settings;
    }

    /**
     * @return The other fighter
     */
    public Fighter getOther() { return battleState.getOther(this); }

    /**
     * @return The battle state
     */
    public BattleState getBattleState() { return battleState; }

    /**
     * Frame update
     */
    public void update() {
        // Change state if needed
        if( nextState != null ){
            machine.changeState(nextState);
            nextState = null;
        }

        // Determine rightmost
        flip = battleState.getOther(this).position.x > position.x;

        // Update fsm
        machine.update();
    }

    /**
     * Render the fighter
     * @param batch
     */
    public void render(SpriteBatch batch){
        machine.render(batch, this);
    }

    /**
     * Render the overlay collision polygons (debug)
     *
     * @param shapeRenderer
     */
    public void renderOverlay(ShapeRenderer shapeRenderer){
        machine.renderOverlay(shapeRenderer);

        // Render brect
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(brect.x, brect.y, brect.width, brect.height);
    }

    /**
     * Constraint movement within screen
     */
    public void constraintMovement() {
        float minX = 30;
        float maxX = getBattleState().getBg().getWidth() - 30;

        if( getPosition().x < minX ){
            getPosition().x = minX;
            setPushing(true);
        }
        if( getPosition().x > maxX ){
            getPosition().x = maxX;
            setPushing(true);
        }
    }

    /**
     * Set the state for next frame to be changed to
     * @param id State identifier
     */
    public void setNextState(String id) {
        nextState = machine.getState(id);
    }

    /**
     * @return Current frame
     */
    public Frame getCurrentFrame() {
        return ((FighterState)machine.getCurrentState()).getCurrentFrame();
    }

    /**
     * @return Bounding rect
     */
    public Rectangle getBrect() {
        return brect;
    }

    /**
     * @return Whether fighter is pushing towards the opponent
     */
    public boolean isPushing() {
        return pushing;
    }

    /**
     * Set pushing state
     * @param pushing
     */
    public void setPushing(boolean pushing) {
        this.pushing = pushing;
    }
}
