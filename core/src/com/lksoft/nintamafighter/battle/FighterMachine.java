package com.lksoft.nintamafighter.battle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.lksoft.nintamafighter.Context;
import com.lksoft.nintamafighter.battle.state.*;
import com.lksoft.nintamafighter.data.FighterData;
import com.lksoft.nintamafighter.data.StateData;
import com.lksoft.nintamafighter.data.collision.CollisionData;

import java.util.HashMap;

/**
 * Created by lake on 15/08/06.
 */
public class FighterMachine extends DefaultStateMachine<Fighter> {
    // States
    private HashMap<String, FighterState> states = new HashMap<>();

    /**
     * Make a new machine
     * @param data
     */
    public FighterMachine(Fighter fighter, FighterData data){
        super(fighter);

        // Load collision
        FileHandle collisionFile = Gdx.files.internal(data.collision);
        CollisionData collisionData = Context.get().json.fromJson(CollisionData.class, collisionFile);

        // Prepare all states
        for( StateData state : data.states ){
            states.put(state.id, createStateFor(state, data, collisionData));
        }
    }

    /**
     * Create state for specified data
     * @param data State data
     * @return A new relevant fighter state
     */
    private FighterState createStateFor(StateData data, FighterData fighterData, CollisionData collisionData) {
        switch (data.id) {
            case "idle": return new Idle(data, collisionData);
            case "walk_fw": return new WalkForward(data, fighterData, collisionData);
            case "walk_bw": return new WalkBackward(data, fighterData, collisionData);
            case "jump_up": return new JumpUp(data, fighterData, collisionData);
            case "light_punch": return new LightPunch(data, collisionData);
            case "damage_high": return new DamageHigh(data, collisionData);
        }
        return null;
    }

    /**
     * Render
     * @param batch
     */
    public void render(SpriteBatch batch, Fighter fighter) {
        ((FighterState)getCurrentState()).draw(batch, fighter);
    }

    /**
     * Debug render overlays (collision polygons)
     * @param shapeRenderer
     */
    public void renderOverlay(ShapeRenderer shapeRenderer){
        ((FighterState)getCurrentState()).drawOverlay(shapeRenderer);
    }

    /**
     * Retrieve state by name
     * @param id State id as specified in json
     * @return The loaded state
     */
    public State<Fighter> getState(String id) {
        return states.get(id);
    }

    /**
     * Sets the new state by id
     * @param state
     */
    public void setState(String state) {
        changeState(getState(state));
    }
}
