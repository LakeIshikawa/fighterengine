package com.lksoft.nintamafighter.battle;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.lksoft.nintamafighter.Context;
import com.lksoft.nintamafighter.NintamaFighter;
import com.lksoft.nintamafighter.data.LayerData;
import com.lksoft.nintamafighter.data.PlaceData;

/**
 * Created by lake on 15/07/28.
 */
public class Background {
    // Data
    private PlaceData placeData;

    // Background layers
    private Array<TextureAtlas.AtlasRegion> bgLayers = new Array<>();
    // Depths
    private Array<Float> depths = new Array<>();
    // Scroll point
    private float scroll;

    /**
     * Create a new bg from place data
     * @param placeData
     */
    public Background(PlaceData placeData){
        this.placeData = placeData;

        // Cache textures
        TextureAtlas atlas = Context.get().assetManager.get("images/images.atlas");
        for( LayerData layer : placeData.layers ){
            bgLayers.add(atlas.findRegion(layer.regionName));
            depths.add(layer.depth);
        }
    }

    /**
     * Update the bg
     * @param fighter1
     * @param fighter2
     */
    void update(Fighter fighter1, Fighter fighter2) {
        // Scroll the camera
        float center = (fighter1.getPosition().x + fighter2.getPosition().x) / 2;
        scroll = center - getWidth()/2;

        float scrollMin = Context.get().viewport.getWorldWidth()/2 - getWidth()/2;
        float scrollMax = getWidth()/2 - Context.get().viewport.getWorldWidth()/2;
        if( scroll < scrollMin ){
            scroll = scrollMin;
        } else if( scroll > scrollMax ){
            scroll = scrollMax;
        }
    }

    /**
     * Render the background layers
     */
    void render(SpriteBatch batch) {
        for(int i=0; i<bgLayers.size; i++){
            float baseX = getWidth()/2 - bgLayers.get(i).originalWidth/2;
            float parallax = scroll * (1-depths.get(i));

            TextureAtlas.AtlasRegion layer = bgLayers.get(i);
            batch.draw(layer, layer.offsetX+baseX-parallax, layer.offsetY);
        }
    }

    public PlaceData getPlaceData() {
        return placeData;
    }

    /**
     * @return Width of the whole bg area
     */
    public int getWidth() {
        return bgLayers.first().originalWidth;
    }

    /**
     * @return Current scroll position (center of screen shift)
     */
    public float getScrollPosition() {
        return scroll;
    }
}
