package com.lksoft.nintamafighter.battle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.lksoft.nintamafighter.data.collision.CollisionType;
import com.lksoft.nintamafighter.data.collision.FixtureData;
import com.lksoft.nintamafighter.data.collision.PolygonData;

/**
 * Created by lake on 15/08/12.
 */
public class CollisionPolygon {

    // Polygons
    private Array<Polygon> polygons = new Array<>();
    // Type
    private CollisionType type;

    /**
     * Create from fixture data
     * @param fixture
     */
    public CollisionPolygon(FixtureData fixture) {
        type = fixture.type;

        for(PolygonData polygonData : fixture.polygons){
            polygons.add(new Polygon(polygonData.points));
        }
    }

    /**
     * @return Collision type
     */
    public CollisionType getType(){
        return type;
    }

    /**
     * @return The polygons
     */
    public Array<Polygon> getPolygons(){
        return polygons;
    }

    /**
     * Update global position
     * @param fighter
     */
    public void update(Fighter fighter, TextureAtlas.AtlasRegion region) {
        for( Polygon polygon : polygons ){
            polygon.setPosition(
                    fighter.getPosition().x - region.originalWidth / 2,
                    fighter.getPosition().y);

            // Flip if needed
            polygon.setOrigin(region.originalWidth / 2, 0);
            polygon.setScale(fighter.isFlip() ? -1 : 1, 1);
        }
    }

    /**
     * Draw the polyon
     * @param shapeRenderer
     */
    public void draw(ShapeRenderer shapeRenderer) {
        // Set proper color
        switch (type){
            case ATTACK: shapeRenderer.setColor(Color.RED); break;
            case HIT_HIGH: shapeRenderer.setColor(Color.BLUE); break;
            case HIT_LOW: shapeRenderer.setColor(Color.GREEN); break;
            case HIT_POINT: shapeRenderer.setColor(Color.WHITE); break;
        }

        for( Polygon polygon : polygons ){
            shapeRenderer.polygon(polygon.getTransformedVertices());
        }
    }

    /**
     * Determine collision with other polygon
     * @param otherPolygon
     * @return
     */
    public boolean collides(CollisionPolygon otherPolygon) {
        for( Polygon polygon : polygons ){
            for( Polygon polygon2 : otherPolygon.polygons ){
                if(Intersector.overlapConvexPolygons(polygon, polygon2)){
                    return true;
                }
            }
        }
        return false;
    }
}
