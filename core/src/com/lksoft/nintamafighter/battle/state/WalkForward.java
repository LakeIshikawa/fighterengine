package com.lksoft.nintamafighter.battle.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.math.Vector2;
import com.lksoft.nintamafighter.battle.Fighter;
import com.lksoft.nintamafighter.battle.FighterState;
import com.lksoft.nintamafighter.data.FighterData;
import com.lksoft.nintamafighter.data.StateData;
import com.lksoft.nintamafighter.data.collision.CollisionData;

/**
 * Created by lake on 15/08/08.
 */
public class WalkForward extends FighterState {

    // Move speed
    private float baseSpeed;
    private Vector2 moveSpeed = new Vector2();

    /**
     * Create new walk state
     * @param data
     */
    public WalkForward(StateData data, FighterData fighterData, CollisionData collisionData) {
        super(data, collisionData);
        baseSpeed = fighterData.speed;
    }

    @Override
    public void stateUpdate(Fighter entity) {
        // Set pushing
        entity.setPushing(true);

        // Get state
        boolean oppRight = entity.getOther().getPosition().x > entity.getPosition().x;
        boolean rkey = Gdx.input.isKeyPressed(entity.getSettings().keys.right);
        boolean lkey = Gdx.input.isKeyPressed(entity.getSettings().keys.left);
        boolean ukey = Gdx.input.isKeyPressed(entity.getSettings().keys.up);

        // No input, back to idle
        if( !rkey && !lkey ){
            entity.setNextState("idle");
            return;
        }

        // Fw to Bw
        if( (oppRight && lkey) || (!oppRight && rkey) ){
            entity.setNextState("walk_bw");
            return;
        }

        // Light punch!
        boolean btn1 = Gdx.input.isKeyPressed(entity.getSettings().keys.bt1);
        if( btn1 ){
            entity.setNextState("light_punch");
            return;
        }

        // Jump
        if( ukey ){
            entity.setNextState("jump_up");
            return;
        }

        // Movement
        moveSpeed.set(baseSpeed, 0);
        moveSpeed.scl(Gdx.graphics.getDeltaTime());
        if( lkey )      entity.getPosition().sub(moveSpeed);
        else if( rkey ) entity.getPosition().add(moveSpeed);
    }

    @Override
    public void exit(Fighter entity) {

    }

    @Override
    public boolean onMessage(Fighter entity, Telegram telegram) {
        return false;
    }
}
