package com.lksoft.nintamafighter.battle.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.msg.Telegram;
import com.lksoft.nintamafighter.battle.Fighter;
import com.lksoft.nintamafighter.battle.FighterState;
import com.lksoft.nintamafighter.data.StateData;
import com.lksoft.nintamafighter.data.collision.CollisionData;

/**
 * Created by lake on 15/08/13.
 */
public class DamageHigh extends FighterState {

    /**
     * Create new high damage state
     * @param data
     * @param collisionData
     */
    public DamageHigh(StateData data, CollisionData collisionData) {
        super(data, collisionData);
    }

    @Override
    public void stateUpdate(Fighter entity) {
        // Go back to idle when finished
        if( animation.isAnimationFinished(timer) ){
            entity.setNextState("idle");
            return;
        }
    }

    @Override
    public void exit(Fighter entity) {

    }

    @Override
    public boolean onMessage(Fighter entity, Telegram telegram) {
        return false;
    }
}
