package com.lksoft.nintamafighter.battle.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.lksoft.nintamafighter.battle.Fighter;
import com.lksoft.nintamafighter.battle.FighterState;
import com.lksoft.nintamafighter.data.FighterData;
import com.lksoft.nintamafighter.data.StateData;
import com.lksoft.nintamafighter.data.collision.CollisionData;

/**
 * Created by lake on 15/08/09.
 */
public class JumpUp extends FighterState {

    // Parameters
    private float jumpTime;
    private int jumpHeight;

    // Move speed
    private float baseSpeed;
    private Vector2 moveSpeed = new Vector2();

    /**
     * New jumpup state
     * @param data
     */
    public JumpUp(StateData data, FighterData fighterData, CollisionData collisionData){
        super(data, collisionData);

        this.jumpTime = fighterData.jumpTime;
        this.jumpHeight = fighterData.jumpHeight;
        baseSpeed = fighterData.speed*3/4;
    }

    @Override
    public void stateUpdate(Fighter entity) {
        boolean rkey = Gdx.input.isKeyPressed(entity.getSettings().keys.right);
        boolean lkey = Gdx.input.isKeyPressed(entity.getSettings().keys.left);

        // Perform jump
        float y;
        if( timer < jumpTime/2 ) {
            y = Interpolation.pow2Out.apply(0, jumpHeight, timer/(jumpTime/2));
        } else {
            y = Interpolation.pow2In.apply(jumpHeight, 0, -1+timer/(jumpTime/2));
        }

        entity.getPosition().y = y;

        // End jump
        if( timer >= jumpTime ){
            entity.getPosition().y = 0;
            entity.setNextState("idle");
            return;
        }

        // Movement
        moveSpeed.set(baseSpeed, 0);
        moveSpeed.scl(Gdx.graphics.getDeltaTime());
        if( lkey )      entity.getPosition().sub(moveSpeed);
        else if( rkey ) entity.getPosition().add(moveSpeed);
    }

    @Override
    public void exit(Fighter entity) {

    }

    @Override
    public boolean onMessage(Fighter entity, Telegram telegram) {
        return false;
    }
}
