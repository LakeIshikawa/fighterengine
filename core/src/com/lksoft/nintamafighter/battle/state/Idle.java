package com.lksoft.nintamafighter.battle.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ai.msg.Telegram;
import com.lksoft.nintamafighter.battle.Fighter;
import com.lksoft.nintamafighter.battle.FighterState;
import com.lksoft.nintamafighter.data.StateData;
import com.lksoft.nintamafighter.data.collision.CollisionData;

/**
 * Created by lake on 15/08/08.
 */
public class Idle extends FighterState {

    /**
     * New idle state
     * @param data
     */
    public Idle(StateData data, CollisionData collisionData){
        super(data, collisionData);
    }

    @Override
    public void stateUpdate(Fighter entity) {
        // Get state
        boolean oppRight = entity.getOther().getPosition().x > entity.getPosition().x;
        boolean rkey = Gdx.input.isKeyPressed(entity.getSettings().keys.right);
        boolean lkey = Gdx.input.isKeyPressed(entity.getSettings().keys.left);
        boolean ukey = Gdx.input.isKeyPressed(entity.getSettings().keys.up);

        // WalkForward!
        if( (oppRight && rkey) || (!oppRight && lkey ) ){
            entity.setNextState("walk_fw");
            return;
        }
        // Walk backward
        if( (!oppRight && rkey) || (oppRight && lkey ) ){
            entity.setNextState("walk_bw");
            return;
        }

        // Jump!
        if( ukey ){
            entity.setNextState("jump_up");
            return;
        }

        // Light punch!
        boolean btn1 = Gdx.input.isKeyPressed(entity.getSettings().keys.bt1);
        if( btn1 ){
            entity.setNextState("light_punch");
            return;
        }
    }

    @Override
    public void exit(Fighter entity) {

    }

    @Override
    public boolean onMessage(Fighter entity, Telegram telegram) {
        return false;
    }
}
