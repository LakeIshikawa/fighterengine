package com.lksoft.nintamafighter.battle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.lksoft.nintamafighter.Context;
import com.lksoft.nintamafighter.NintamaFighter;
import com.lksoft.nintamafighter.data.StateData;
import com.lksoft.nintamafighter.data.collision.BodyData;
import com.lksoft.nintamafighter.data.collision.CollisionData;

import java.util.HashMap;

/**
 * Created by lake on 15/08/06.
 */
public abstract class FighterState implements State<Fighter> {

    // Animation
    protected Animation animation;
    // Timer
    protected float timer;

    /**
     * Create new state
     * @param data
     */
    public FighterState(StateData data, CollisionData collisionData) {
        TextureAtlas atlas = Context.get().assetManager.get("images/images.atlas");

        // Get all frames
        Array<TextureAtlas.AtlasRegion> regions = atlas.findRegions(data.regions);
        // Create hash of bodies
        HashMap<String, BodyData> bodyHash = new HashMap<>();
        for( BodyData bodyData : collisionData.bodies ){
            bodyHash.put(bodyData.name, bodyData);
        }

        // Create frames
        Array<Frame> frames = new Array<>();
        for(int idx : data.sequence ){
            TextureAtlas.AtlasRegion region = regions.get(idx);
            String regionName = region.name + String.format("_%02d", region.index);
            frames.add(new Frame(region, bodyHash.get(regionName)));
        }

        animation = new Animation(data.frameDelay, frames,
                data.loop? Animation.PlayMode.LOOP: Animation.PlayMode.NORMAL);
    }

    @Override
    public void update(Fighter entity){
        timer += Gdx.graphics.getDeltaTime();

        // Reset pushing state
        entity.setPushing(false);

        // Update polygons position
        Frame frame = getCurrentFrame();
        if( frame.update(entity) ) return;

        // Process state
        stateUpdate(entity);

        // Constraint movement
        entity.constraintMovement();
    }

    /**
     * State update interface
     * @param entity
     */
    public abstract void stateUpdate(Fighter entity);

    /**
     * Draw
     * @param batch
     */
    public void draw(SpriteBatch batch, Fighter fighter) {
        Frame frame = getCurrentFrame();

        // Draw frame
        batch.draw(
                frame,
                frame.offsetX + fighter.getPosition().x - frame.originalWidth / 2,
                frame.offsetY + fighter.getPosition().y
        );
    }

    /**
     * Draw overlay collision polygons
     * @param shapeRenderer
     */
    public void drawOverlay(ShapeRenderer shapeRenderer) {
        Frame frame = getCurrentFrame();

        // Draw polygons
        frame.drawOverlay(shapeRenderer);
    }

    @Override
    public void enter(Fighter entity) {
        reset();
    }

    /**
     * Reset to start
     */
    public void reset() {
        timer = 0;
    }

    /**
     * @return Current animation frame
     */
    public Frame getCurrentFrame() {
        return (Frame) animation.getKeyFrame(timer);
    }
}
