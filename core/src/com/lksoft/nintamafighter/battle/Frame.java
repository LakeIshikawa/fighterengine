package com.lksoft.nintamafighter.battle;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.lksoft.nintamafighter.data.collision.BodyData;
import com.lksoft.nintamafighter.data.collision.CollisionType;
import com.lksoft.nintamafighter.data.collision.FixtureData;

/**
 * Created by lake on 15/08/12.
 */
public class Frame extends TextureAtlas.AtlasRegion {

    // Collision polygons
    private Array<CollisionPolygon> polygons = new Array<>();

    /**
     * Create a new frame with collision data
     * @param region The atlas region for this frame
     */
    public Frame(TextureAtlas.AtlasRegion region, BodyData body) {
        super(region);

        // Load polygons
        if( body != null ) {
            for (FixtureData fixture : body.fixtures) {
                polygons.add(new CollisionPolygon(fixture));
            }
        }
    }

    /**
     * Update state from fighter
     * @param fighter
     */
    public boolean update(Fighter fighter) {
        // Update region
        if( fighter.isFlip() && !isFlipX() ) flip(true, false);
        if (!fighter.isFlip() && isFlipX() ) flip(true, false);

        // Update polygon
        for( CollisionPolygon polygon : polygons ){
            polygon.update(fighter, this);
        }

        // Process collisions
        CollisionType damage = receivesDamage(fighter.getOther().getCurrentFrame());
        if( damage == null ) return false;

        switch( damage ){
            case HIT_HIGH:
                fighter.setNextState("damage_high");
                return true;
        }

        // Should not happen
        return false;
    }

    /**
     * Draw overlay collision polygons
     * @param shapeRenderer
     */
    public void drawOverlay(ShapeRenderer shapeRenderer) {
        // Update polygon
        for( CollisionPolygon polygon : polygons ){
            polygon.draw(shapeRenderer);
        }
    }

    /**
     * Determines if this frame receives damage from the specified frame
     * @param otherFrame
     * @return
     */
    private CollisionType receivesDamage(Frame otherFrame) {
        for( CollisionPolygon polygon : polygons ){
            if( polygon.getType().isHitType() ){
                // For all attack polygons of other type
                for( CollisionPolygon otherPolygon : otherFrame.polygons ) {
                    if( otherPolygon.getType() == CollisionType.ATTACK
                            && polygon.collides(otherPolygon)) return polygon.getType();
                }
            }
        }
        return null;
    }
}
