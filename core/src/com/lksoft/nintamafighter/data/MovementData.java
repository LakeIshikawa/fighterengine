package com.lksoft.nintamafighter.data;

/**
 * Created by lake on 15/08/06.
 */
public class MovementData {
    // Movement forward
    public float df;
    // Movement upward
    public float dy;
    // Speed
    public float speed = 40;
    // Looping
    public boolean loop;
}
