package com.lksoft.nintamafighter.data.collision;

import com.badlogic.gdx.utils.Array;

/**
 * Created by lake on 15/08/10.
 */
public class FixtureData {
    public CollisionType type;
    public Array<PolygonData> polygons;
}
