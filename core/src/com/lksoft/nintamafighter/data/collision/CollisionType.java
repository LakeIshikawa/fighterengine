package com.lksoft.nintamafighter.data.collision;

/**
 * Created by lake on 15/08/01.
 */
public enum CollisionType {
    HIT_HIGH(true),
    HIT_LOW(true),
    ATTACK(false),
    HIT_POINT(false);

    boolean hit;
    CollisionType(boolean hit){
        this.hit = hit;
    }

    /**
     * @return Whether the type is a hit type
     */
    public boolean isHitType(){
        return hit;
    }
}
