package com.lksoft.nintamafighter.data.collision;

import com.badlogic.gdx.utils.Array;

/**
 * Created by lake on 15/08/10.
 */
public class BodyData {
    public String name;
    public Array<FixtureData> fixtures;
}
