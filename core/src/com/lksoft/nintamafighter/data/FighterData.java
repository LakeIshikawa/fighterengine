package com.lksoft.nintamafighter.data;

import com.badlogic.gdx.utils.Array;

/**
 * Created by lake on 15/07/26.
 */
public class FighterData {
    // Display name
    public String name;
    // Speed
    public float speed;
    // Jump time
    public float jumpTime = 1.3f;
    // Jump height
    public int jumpHeight = 130;

    // States
    public Array<StateData> states = new Array<>();
    // Collision resource
    public String collision;
}
