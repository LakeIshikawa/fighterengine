package com.lksoft.nintamafighter.data;

import com.badlogic.gdx.utils.Array;

/**
 * Created by lake on 15/07/26.
 */
public class PlaceData {
    // Name
    public String name;
    // Bg layers
    public Array<LayerData> layers = new Array<>();
}
