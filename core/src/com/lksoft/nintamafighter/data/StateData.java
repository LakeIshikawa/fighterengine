package com.lksoft.nintamafighter.data;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by lake on 15/07/26.
 */
public class StateData {
    // For visual representation of the state
    public Vector2 position = new Vector2();

    // Motion identifier name
    public String id;
    // Regions
    public String regions;
    // Animation sequence
    public Array<Integer> sequence = new Array<>();
    // Frame delay
    public float frameDelay = 0.1f;
    // Loop or not
    public boolean loop;
}
