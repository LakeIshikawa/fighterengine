package com.lksoft.nintamafighter.data;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.ReflectionException;

/**
 * Created by lake on 15/08/08.
 */
public class KeySettingsDataSerializer implements Json.Serializer<KeySettingsData> {

    @Override
    public void write(Json json, KeySettingsData object, Class knownType) {

    }

    @Override
    public KeySettingsData read(Json json, JsonValue jsonData, Class type) {
        // Prepare Input.Key class
        try {
            Class keysCls = ClassReflection.forName("com.badlogic.gdx.Input$Keys");

            KeySettingsData data = new KeySettingsData();
            String up = jsonData.getString("up");
            String down = jsonData.getString("down");
            String left = jsonData.getString("left");
            String right = jsonData.getString("right");
            String bt1 = jsonData.getString("btn1");
            String bt2 = jsonData.getString("btn2");
            String bt3 = jsonData.getString("btn3");

            data.up = (int) keysCls.getField(up).get(null);
            data.down = (int) keysCls.getField(down).get(null);
            data.left = (int) keysCls.getField(left).get(null);
            data.right = (int) keysCls.getField(right).get(null);
            data.bt1 = (int) keysCls.getField(bt1).get(null);
            data.bt2 = (int) keysCls.getField(bt2).get(null);
            data.bt3 = (int) keysCls.getField(bt3).get(null);

            return data;

        } catch (ReflectionException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
