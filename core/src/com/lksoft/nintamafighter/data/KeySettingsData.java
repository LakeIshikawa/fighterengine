package com.lksoft.nintamafighter.data;

/**
 * Created by lake on 15/08/08.
 */
public class KeySettingsData {
    public int up;
    public int down;
    public int left;
    public int right;
    public int bt1;
    public int bt2;
    public int bt3;
}
