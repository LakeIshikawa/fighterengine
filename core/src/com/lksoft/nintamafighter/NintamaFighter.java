package com.lksoft.nintamafighter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.lksoft.nintamafighter.screen.LoadingScreen;

public class NintamaFighter extends Game {

	@Override
	public void create() {
        Context.self = new Context(this);
		setScreen(new LoadingScreen());
	}
}
