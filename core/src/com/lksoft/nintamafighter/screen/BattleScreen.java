package com.lksoft.nintamafighter.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.lksoft.nintamafighter.Context;
import com.lksoft.nintamafighter.NintamaFighter;
import com.lksoft.nintamafighter.battle.BattleRenderer;
import com.lksoft.nintamafighter.battle.BattleState;
import com.lksoft.nintamafighter.data.FighterData;
import com.lksoft.nintamafighter.data.PlaceData;
import com.lksoft.nintamafighter.data.SettingsData;

/**
 * Created by lake on 15/07/26.
 */
public class BattleScreen implements Screen {

    // Battle state
    private BattleState battleState;
    // Battle renderer
    private BattleRenderer battleRenderer;

    // Settings
    private SettingsData settings;

    /**
     * Create battle screen for specified place and fighters
     * @param place
     * @param f1
     * @param f2
     * @param settings
     */
    public BattleScreen(PlaceData place, FighterData f1, FighterData f2, SettingsData settings) {
        Context.get().viewport =  new FillViewport(640, 480);
        battleState = new BattleState(place, f1, f2, settings);
        battleRenderer = new BattleRenderer(Context.get().viewport, battleState);
        this.settings = settings;
    }

    @Override
    public void show() {
        // Center camera
        int bgWidth = battleState.getBg().getWidth();
        Context.get().viewport.getCamera().position.set(bgWidth/2, Context.get().viewport.getWorldHeight()/2, 0);
        Context.get().viewport.getCamera().update();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0.5f, 1.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Function keys
        if(Gdx.input.isKeyJustPressed(Input.Keys.F1)){
            battleRenderer.setRenderOverlay(!battleRenderer.isRenderOverlay());
        }

        // Progress the battle
        battleState.update();

        // Scroll camera
        battleRenderer.updateCamera();

        // Render
        battleRenderer.render();
    }

    @Override
    public void resize(int width, int height) {
        Context.get().viewport.update(width, height, false);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
