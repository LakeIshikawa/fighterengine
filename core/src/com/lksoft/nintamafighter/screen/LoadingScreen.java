package com.lksoft.nintamafighter.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Json;
import com.lksoft.nintamafighter.Context;
import com.lksoft.nintamafighter.NintamaFighter;
import com.lksoft.nintamafighter.data.FighterData;
import com.lksoft.nintamafighter.data.PlaceData;
import com.lksoft.nintamafighter.data.SettingsData;

/**
 * Created by lake on 15/07/26.
 */
public class LoadingScreen implements Screen {

    @Override
    public void show() {
        // Load image atlas
        Context.get().assetManager.load("images/images.atlas", TextureAtlas.class);
    }

    @Override
    public void render(float delta) {
        if( Context.get().assetManager.update() ){
            startBattle();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    /**
     * Start a test battle
     */
    public void startBattle(){
        // Read settings
        SettingsData settings = Context.get().json.fromJson(SettingsData.class, Gdx.files.internal("data/settings.json"));

        // Create some fighters
        PlaceData place = Context.get().json.fromJson(PlaceData.class, Gdx.files.internal("data/places/sanfrancisco.json"));
        FighterData f1 = Context.get().json.fromJson(FighterData.class, Gdx.files.internal("data/fighters/ken.json"));

        Context.get().game.setScreen(new BattleScreen(place, f1, f1, settings));
    }
}
