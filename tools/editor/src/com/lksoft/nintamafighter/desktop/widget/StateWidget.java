package com.lksoft.nintamafighter.desktop.widget;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.kotcrab.vis.ui.widget.MenuItem;
import com.kotcrab.vis.ui.widget.PopupMenu;
import com.lksoft.nintamafighter.data.StateData;
import com.lksoft.nintamafighter.desktop.gui.EditorGUI;

/**
 * Created by lake on 15/07/31.
 */
public class StateWidget extends Actor {
    // State
    private StateData state;

    // Rendering
    private NinePatchDrawable ninePatch;
    private BitmapFont font;

    // Touch
    private Vector2 touchPoint;
    private Vector2 parent = new Vector2();

    // Context menu
    private PopupMenu menu = new PopupMenu();

    /**
     * Create state widget for editing state data
     * @param state
     */
    public StateWidget(final StateData state) {
        this.state = state;

        // Create menu
        menu.addItem(new MenuItem("Edit", new ChangeListener(){
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                EditorGUI.gui.editState(state);
            }
        }));


        // Input listener
        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.RIGHT) {
                    menu.showMenu(getStage(), event.getStageX(), event.getStageY());
                    touchPoint = null;
                } else {
                    touchPoint = new Vector2();
                    touchPoint.set(x, y);
                }
                return true;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if( touchPoint == null ) return;

                parent.set(0, 0);
                getParent().localToStageCoordinates(parent);
                float nx = event.getStageX() - parent.x - touchPoint.x;
                float ny = event.getStageY() - parent.y - touchPoint.y;
                setPosition(nx, ny);
                state.position.set(nx, ny);

                // Set dirty
                EditorGUI.gui.getTabs().getActiveTab().setDirty(true);
            }
        });

        Skin skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        ninePatch = new NinePatchDrawable(skin.getPatch("default-round"));
        font = skin.getFont("default-font");

        GlyphLayout layout = new GlyphLayout(font, state.id);
        setBounds(state.position.x, state.position.y, layout.width+20, layout.height+20);
    }

    @Override
    public void draw (Batch batch, float parentAlpha) {
        batch.setColor(getColor());
        ninePatch.draw(batch, getX(), getY(), getWidth(), getHeight());
        font.draw(batch, state.id, getX()+10, getY()+getHeight()-10);
    }
}
