package com.lksoft.nintamafighter.desktop.widget;

import com.kotcrab.vis.ui.widget.VisTable;
import com.lksoft.nintamafighter.data.FighterData;
import com.lksoft.nintamafighter.data.StateData;

/**
 * Created by lake on 15/07/31.
 */
public class StateMachineEditor extends VisTable {

    // State
    private FighterData data;

    /**
     * Create new state machine editor on fighter data
     * @param data
     */
    public StateMachineEditor(FighterData data) {
        this.data = data;

        for(StateData state : data.states) {
            addActor(new StateWidget(state));
        }
    }
}
