package com.lksoft.nintamafighter.desktop.widget;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by lake on 15/08/02.
 */
public class FrameEditor extends Actor {
    // State
    //private FrameData frameData;

    // Draw
    private Texture image;

//    public FrameEditor(FrameData data){
//        this.frameData = data;
//
//
//        setBounds(0, 0, image.getWidth(), image.getHeight());
//    }

    @Override
    public void draw (Batch batch, float parentAlpha) {
        batch.draw(image, 0, 0);
    }
}
