package com.lksoft.nintamafighter.desktop.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.Menu;
import com.kotcrab.vis.ui.widget.MenuBar;
import com.kotcrab.vis.ui.widget.MenuItem;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.file.FileChooser;
import com.kotcrab.vis.ui.widget.file.FileChooserAdapter;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPane;
import com.lksoft.nintamafighter.data.StateData;

/**
 * Created by lake on 15/07/30.
 */
public class EditorGUI {
    // Singleton
    public static EditorGUI gui;

    // The stage
    private Stage stage;

    // Root table
    private Table root;
    private Table contents;

    // Windows
    private ProjectWindow projectWindow;

    // File choosers
    private FileChooser openProject = new FileChooser(FileChooser.Mode.OPEN);

    /**
     * Create editor GUI
     * @param stage
     */
    public EditorGUI(Stage stage) {
        this.stage = stage;
    }

    /**
     * Create the main editor GUI
     */
    public void createGUI() {
        gui = this;

        // Create root
        root = new Table();
        root.setFillParent(true);
        stage.addActor(root);

        // Menu bar
        createMenuBar();

        // Create navigator window
        projectWindow = new ProjectWindow(stage);
        stage.addActor(projectWindow);
    }

    /**
     * @return The diting tabbed pane
     */
    public TabbedPane getTabs() {
        return projectWindow.getTabbedPane();
    }

    /**
     * Fire up editor for specified state
     * @param state
     */
    public void editState(StateData state) {
        projectWindow.createEditorTab(state);
    }

    /**
     * Create the menu bar
     */
    private void createMenuBar() {
        MenuBar bar = new MenuBar();

        // File
        Menu menu = new Menu("Project");
        bar.addMenu(menu);

        MenuItem newProject = new MenuItem("New");
        newProject.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                newProject();
            }
        });

        MenuItem openProject = new MenuItem("Open");
        openProject.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                openProject();
            }
        });

        MenuItem saveProject = new MenuItem("Save");
        saveProject.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                saveProject();
            }
        });


        MenuItem exit = new MenuItem("Exit");
        exit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });

        menu.addItem(newProject);
        menu.addItem(openProject);
        menu.addItem(saveProject);
        menu.addItem(exit);

        // Add menu bar to stage
        root.add(bar.getTable()).fillX().expandX().row();
        contents = new VisTable();
        root.add(contents).expand().fill().row();
    }

    /**
     * Create and open new project
     */
    private void newProject() {

    }

    /**
     * Open project
     */
    private void openProject() {
        openProject.setSelectionMode(FileChooser.SelectionMode.DIRECTORIES);
        openProject.setMultiselectionEnabled(false);
        openProject.setDirectory("../../../");
        openProject.setListener(new FileChooserAdapter() {
            @Override
            public void selected(FileHandle file) {
                projectWindow.openProject(file);
            }
        });

        stage.addActor(openProject);
    }

    /**
     * Save project
     */
    private void saveProject() {
        // Save all files
        projectWindow.saveProject();
    }
}
