package com.lksoft.nintamafighter.desktop.gui;

import com.kotcrab.vis.ui.widget.VisTable;
import com.lksoft.nintamafighter.data.StateData;

/**
 * Created by lake on 15/08/01.
 */
public class PropTable extends VisTable {
    // State
    private StateData state;

    /**
     * Create prop table
     * @param state
     */
    public PropTable(StateData state) {
        this.state = state;
    }
}
