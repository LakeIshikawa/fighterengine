package com.lksoft.nintamafighter.desktop.gui;

import com.kotcrab.vis.ui.widget.VisTable;
import com.lksoft.nintamafighter.data.StateData;
import com.lksoft.nintamafighter.desktop.widget.FrameEditor;

/**
 * Created by lake on 15/08/01.
 */
public class FrameTable extends VisTable {
    // State
    private StateData state;

    // Widgets
    private FrameEditor frameEditor;
    private int currentFrame = 0;


    /**
     * Create frame editor
     * @param state
     */
    public FrameTable(StateData state) {
        this.state = state;

    }
}
