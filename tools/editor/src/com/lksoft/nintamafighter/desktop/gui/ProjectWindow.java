package com.lksoft.nintamafighter.desktop.gui;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.*;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPane;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPaneAdapter;
import com.lksoft.nintamafighter.data.StateData;
import com.lksoft.nintamafighter.desktop.NintamaEditor;
import com.lksoft.nintamafighter.desktop.project.FighterFile;
import com.lksoft.nintamafighter.desktop.project.PlaceFile;
import com.lksoft.nintamafighter.desktop.project.Project;

/**
 * Created by lake on 15/07/30.
 */
public class ProjectWindow extends VisWindow {

    // Top nodes
    private Tree.Node fighters = new Tree.Node(new VisLabel("fighters"));
    private Tree.Node places = new Tree.Node(new VisLabel("places"));

    // Tabbed pane
    private TabbedPane tabbedPane;

    // Stage
    private Stage stage;

    // Project
    private Project project;

    /**
     * Create navigation window!
     */
    public ProjectWindow(Stage stage) {
        super("Project");
        this.stage = stage;

        // Bounds
        setBounds(0, 0, NintamaEditor.SCREEN_W, NintamaEditor.SCREEN_H - 50);
        setMovable(false);

        // Create tree
        VisTable contents = new VisTable();
        VisScrollPane scrollPane = new VisScrollPane(contents);
        final VisTree tree = new VisTree();
        contents.add(tree).expand().fill();

        // Create tabs
        tabbedPane = new TabbedPane();
        final VisTable editorContents = new VisTable();
        tabbedPane.addListener(new TabbedPaneAdapter() {
            @Override
            public void switchedTab(Tab tab) {
                editorContents.clearChildren();

                if (tab != null) {
                    Table content = tab.getContentTable();
                    editorContents.add(content).expand().fill();
                }
            }

            @Override
            public void removedAllTabs () {
                editorContents.clearChildren();
            }
        });

        VisTable editorTable = new VisTable();
        editorTable.add(tabbedPane.getTable()).expandX().fillX().top().row();
        editorTable.add(editorContents).expand().fill();

        // Create contents
        VisSplitPane splitPane = new VisSplitPane(scrollPane, editorTable, false);
        splitPane.setSplitAmount(0.1f);
        splitPane.setMinSplitAmount(0.05f);
        splitPane.setMaxSplitAmount(0.2f);
        add(splitPane).expand().fill();

        // Load tree
        tree.add(fighters);
        tree.add(places);

        // Tree selection
        tree.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!tree.getSelection().isEmpty()) {
                    editNode(tree.getSelection().first());
                }
            }
        });
    }

    /**
     * Open project
     * @param projectRoot
     */
    public void openProject(FileHandle projectRoot){
        // Load project
        project = new Project(projectRoot);

        // Create navigator tree
        for(FighterFile f : project.fighters){
            Tree.Node node = new Tree.Node(new VisLabel(f.data.name));
            node.setObject(f);
            fighters.add(node);
        }
        for(PlaceFile f : project.places){
            Tree.Node node = new Tree.Node(new VisLabel(f.data.name));
            node.setObject(f);
            places.add(node);
        }
    }

    /**
     * @return The tabbed pane
     */
    public TabbedPane getTabbedPane() {
        return tabbedPane;
    }

    /**
     * Open editor window for selected node
     * @param node
     */
    private void editNode(Tree.Node node) {
        // Fighter
        if( node.getParent() == fighters ){
            createEditorTab((FighterFile)node.getObject());
        }
        else if( node.getParent() == places ){
            createEditorTab((PlaceFile)node.getObject());
        }
    }

    /**
     * Create editor tab for place data
     * @param placeFile
     */
    private void createEditorTab(PlaceFile placeFile) {
        for( Tab tab : tabbedPane.getTabs() ){
            if( tab instanceof PlaceEditorTab ){
                if( ((PlaceEditorTab)tab).getPlaceFile() == placeFile ){
                    tabbedPane.switchTab(tab);
                    return;
                }
            }
        }

        // Create new
        tabbedPane.add(new PlaceEditorTab(placeFile));
    }

    /**
     * Crete editor tab for fighter
     * @param fighterFile
     */
    private void createEditorTab(FighterFile fighterFile) {
        for( Tab tab : tabbedPane.getTabs() ){
            if( tab instanceof FighterEditorTab ){
                if( ((FighterEditorTab)tab).getFighterFile() == fighterFile ){
                    tabbedPane.switchTab(tab);
                    return;
                }
            }
        }

        tabbedPane.add(new FighterEditorTab(fighterFile));
    }

    /**
     * Crete editor tab for state
     * @param state
     */
    public void createEditorTab(StateData state) {
        for( Tab tab : tabbedPane.getTabs() ){
            if( tab instanceof StateEditorTab ){
                if( ((StateEditorTab)tab).getState() == state ){
                    tabbedPane.switchTab(tab);
                    return;
                }
            }
        }

        tabbedPane.add(new StateEditorTab(state));
    }

    /**
     * Save all edits
     */
    public void saveProject() {
        if( project == null ) return;
        project.saveAll();

        // Undirty all tabs
        for( Tab t : tabbedPane.getTabs() ){
            t.setDirty(false);
        }
    }
}
