package com.lksoft.nintamafighter.desktop.gui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisSplitPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;
import com.lksoft.nintamafighter.desktop.project.FighterFile;
import com.lksoft.nintamafighter.desktop.widget.StateMachineEditor;

/**
 * Created by lake on 15/07/30.
 */
public class FighterEditorTab extends Tab {
    // Data
    private FighterFile fighterFile;

    // UI
    private VisTable content;

    /**
     * Create new fighter editor tab
     * @param fighterFile
     */
    public FighterEditorTab(FighterFile fighterFile) {
        super(true, true);
        this.fighterFile = fighterFile;

        Actor stateMachine = new StateMachineEditor(fighterFile.data);
        VisTable inspector = new VisTable();
        content = new VisTable();
        VisSplitPane pane = new VisSplitPane(stateMachine, inspector, true);
        pane.setSplitAmount(0.8f);
        content.add(pane).expand().fill();
    }

    /**
     * @return Fighter data
     */
    public FighterFile getFighterFile() {
        return fighterFile;
    }

    @Override
    public String getTabTitle() {
        return fighterFile.data.name;
    }

    @Override
    public Table getContentTable() {
        return content;
    }
}
