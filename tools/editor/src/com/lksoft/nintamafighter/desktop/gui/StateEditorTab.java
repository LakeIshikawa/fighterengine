package com.lksoft.nintamafighter.desktop.gui;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisSplitPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;
import com.lksoft.nintamafighter.data.StateData;

/**
 * Created by lake on 15/08/01.
 */
public class StateEditorTab extends Tab {
    // Data
    private StateData state;

    // UI
    private VisTable content;

    /**
     * Create new place editor tab
     * @param state
     */
    public StateEditorTab(StateData state) {
        super(true, true);
        this.state = state;
        content = new VisTable();

        // Frame editor
        FrameTable frameTable = new FrameTable(state);
        PropTable propTable = new PropTable(state);

        VisSplitPane splitPane = new VisSplitPane(frameTable, propTable, false);
        content.add(splitPane).expand().fill();
    }

    /**
     * @return Place data
     */
    public StateData getState(){
        return state;
    }

    @Override
    public String getTabTitle() {
        return state.id;
    }

    @Override
    public Table getContentTable() {
        return content;
    }
}
