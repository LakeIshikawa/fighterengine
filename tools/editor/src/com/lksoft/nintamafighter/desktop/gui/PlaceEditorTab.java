package com.lksoft.nintamafighter.desktop.gui;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;
import com.lksoft.nintamafighter.desktop.project.PlaceFile;

/**
 * Created by lake on 15/07/30.
 */
public class PlaceEditorTab extends Tab {
    // Data
    private PlaceFile palceFile;

    // UI
    private VisTable content;

    /**
     * Create new place editor tab
     * @param palceFile
     */
    public PlaceEditorTab(PlaceFile palceFile) {
        super(true, true);
        this.palceFile = palceFile;
        content = new VisTable();
        content.add(new VisLabel("Editing "+palceFile.data.name));
    }

    /**
     * @return Place data
     */
    public PlaceFile getPlaceFile(){
        return palceFile;
    }

    @Override
    public String getTabTitle() {
        return palceFile.data.name;
    }

    @Override
    public Table getContentTable() {
        return content;
    }
}
