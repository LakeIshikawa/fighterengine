package com.lksoft.nintamafighter.desktop;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.kotcrab.vis.ui.VisUI;
import com.lksoft.nintamafighter.desktop.gui.EditorGUI;

/**
 * Created by lake on 15/07/30.
 */
public class NintamaEditor implements ApplicationListener {

    // Stage2d
    private Stage stage;

    // Editor GUI
    private EditorGUI editorGUI;

    // Viewport size
    public static int SCREEN_W;
    public static int SCREEN_H;

    @Override
    public void create() {
        // Load visUI
        VisUI.load();

        // Create stage
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);

        // Create gui
        editorGUI = new EditorGUI(stage);
        editorGUI.createGUI();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0.5f, 1.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
