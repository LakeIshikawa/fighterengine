package com.lksoft.nintamafighter.desktop;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

/**
 * Created by lake on 15/07/26.
 */
public class DataUtils {

    /**
     * Creates assets from specified resources folder, and place them at
     * specified destination
     *
     * @param resources
     * @param destination
     */
    public static void makeAssets(String resources, String destination){
        FileHandle dest = new FileHandle(destination);
        dest.deleteDirectory();

        // Copy all data
        FileHandle collisionData = new FileHandle(resources+"/collisions/data");
        collisionData.copyTo(new FileHandle(destination+"/collisions"));
        FileHandle data = new FileHandle(resources+"/data");
        data.copyTo(new FileHandle(destination+"/data"));

        // Pack images
        TexturePacker.Settings settings = new TexturePacker.Settings();
        settings.maxWidth = 4096;
        settings.maxHeight = 4096;
        settings.filterMin = Texture.TextureFilter.Linear;
        settings.filterMag = Texture.TextureFilter.Linear;
        settings.stripWhitespaceX = true;
        settings.stripWhitespaceY = true;
        settings.flattenPaths = true;

        TexturePacker.process(settings, resources+"/images/", destination+"/images", "images");
    }
}
