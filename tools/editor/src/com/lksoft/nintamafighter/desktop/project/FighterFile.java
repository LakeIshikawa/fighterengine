package com.lksoft.nintamafighter.desktop.project;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;
import com.lksoft.nintamafighter.data.FighterData;

/**
 * Created by lake on 15/08/01.
 */
public class FighterFile {
    public FileHandle file;
    public FighterData data;

    private Json json = new Json();

    /**
     * Create new fighter file
     * @param file
     * @param data
     */
    public FighterFile(FileHandle file, FighterData data){
        this.file = file;
        this.data = data;

        json.setIgnoreUnknownFields(true);
    }

    /**
     * Save changes
     */
    public void save(){
        json.toJson(data, file);
    }
}
