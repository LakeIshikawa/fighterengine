package com.lksoft.nintamafighter.desktop.project;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.lksoft.nintamafighter.data.FighterData;
import com.lksoft.nintamafighter.data.PlaceData;

/**
 * Created by lake on 15/08/01.
 */
public class Project {
    // Project data
    public FileHandle root;
    public Array<FighterFile> fighters = new Array<>();
    public Array<PlaceFile> places = new Array<>();

    // Json
    private Json json = new Json();

    /**
     * Load project
     * @param root
     */
    public Project(FileHandle root){
        this.root = root;

        json.setIgnoreUnknownFields(true);
        loadFighters();
        loadPlaces();
    }

    /**
     * Load fighters from project files
     */
    private void loadFighters() {
        FileHandle handle = new FileHandle(root.path()+"/data/fighters");
        if( !handle.exists() ) return;

        for( FileHandle f : handle.list() ){
            // Parse it
            FighterData data = json.fromJson(FighterData.class, f);
            fighters.add(new FighterFile(f, data));
        }
    }

    /**
     * Load places from project files
     */
    private void loadPlaces() {
        FileHandle handle = new FileHandle(root.path()+"/data/places");
        if( !handle.exists() ) return;

        for( FileHandle f : handle.list() ){
            // Parse it
            PlaceData data = json.fromJson(PlaceData.class, f);
            Tree.Node node = new Tree.Node(new VisLabel(data.name));
            node.setObject(data);
            places.add(new PlaceFile(f, data));
        }
    }

    /**
     * Save all changes
     */
    public void saveAll() {
        for( FighterFile f : fighters ){
            f.save();
        }
        for( PlaceFile f : places ){
            f.save();
        }
    }
}
