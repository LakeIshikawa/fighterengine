package com.lksoft.nintamafighter.desktop.project;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;
import com.lksoft.nintamafighter.data.PlaceData;

/**
 * Created by lake on 15/08/01.
 */
public class PlaceFile {
    public FileHandle file;
    public PlaceData data;

    private Json json = new Json();

    /**
     * Create new place file
     * @param file
     * @param data
     */
    public PlaceFile(FileHandle file, PlaceData data){
        this.file = file;
        this.data = data;

        json.setIgnoreUnknownFields(true);
    }

    /**
     * Save changes
     */
    public void save(){
        json.toJson(data, file);
    }
}
